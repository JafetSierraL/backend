from django.contrib import admin
from authApp import views
from django.urls import path, include
from django.contrib.sites.models import Site

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', views.AccountListCreateView.as_view()),
    path('account/<int:pk>', views.AccountRetrieveUpdateDestroy.as_view()),
    path('records/',views.RecordListCreateView.as_view()),
    path('results/',views.ResultListCreateView.as_view()),
    path('record/<int:pk>', views.RecordRetrieveUpdateDestroy.as_view()),
    path('result/<int:pk>', views.ResutlRetrieveUpdateDestroy.as_view()),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls'))
      
]

admin.site.unregister(Site)