from django.db import models
from .record import Record
from datetime import datetime

class Result(models.Model):
    resultId = models.AutoField(primary_key=True)
    result_record = models.ForeignKey( Record, related_name="my_data", on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(default=datetime.now, blank=True)
    result = models.BooleanField(blank=False,default=0)