from django.db import models

class Account(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(null=True)
    email = models.TextField(null=True)
    
