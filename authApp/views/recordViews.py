from rest_framework import views, status
from rest_framework.response import Response
from rest_framework import generics

from authApp.models import Record

from authApp.serializers import RecordSerializer

class RecordView(views.APIView):
    
    def get(self,request):
        queryset = Record.objects.all()
        serializer = RecordSerializer(queryset, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
    
    def post(self,request):
        print("Información obtenida", request.data)
        serializer = RecordSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = {"message": "Se añadio la informacion"}
            return Response(data=data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class RecordListCreateView(generics.ListCreateAPIView):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer
    
class RecordRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer