from rest_framework import fields, serializers
from authApp.models.record import Record

class RecordSerializer(serializers.ModelSerializer):
    class Meta:
       model =  Record
       fields = ['recordId','age','pregnancies','glucose','bloodpreassure','insulin','bmi','tskinth']
       